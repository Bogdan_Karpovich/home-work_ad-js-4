 const style = document.createElement('style');
 style.textContent = `
   .film {
     margin-bottom: 20px;
   }
   .loading {
     font-style: italic;
     color: grey;
   }
   .spinner {
     border: 4px solid rgba(0, 0, 0, 0.1);
     border-left-color: #000;
     border-radius: 50%;
     width: 24px;
     height: 24px;
     animation: spin 1s linear infinite;
     display: inline-block;
   }
   @keyframes spin {
     to {
       transform: rotate(360deg);
     }
   }
 `;
 document.head.appendChild(style);

 const filmsContainer = document.createElement('div');
 filmsContainer.id = 'films-container';
 document.body.appendChild(filmsContainer);

 function createFilmElement(film) {
   const filmElement = document.createElement('div');
   filmElement.className = 'film';
   filmElement.innerHTML = `
     <h2>Episode ${film.episodeId}: ${film.name}</h2>
     <p>${film.openingCrawl}</p>
     <div class="characters">
       <div class="loading">
         Loading characters...
         <div class="spinner"></div>
       </div>
     </div>
   `;
   return filmElement;
 }

 function createCharactersList(characters) {
   const ul = document.createElement('ul');
   characters.forEach(character => {
     const li = document.createElement('li');
     li.textContent = character.name;
     ul.appendChild(li);
   });
   return ul;
 }

 function fetchCharacters(characterUrls) {
   return Promise.all(characterUrls.map(url => fetch(url).then(response => response.json())));
 }

 fetch('https://ajax.test-danit.com/api/swapi/films')
   .then(response => response.json())
   .then(films => {
     films.sort((a, b) => a.episodeId - b.episodeId);

     films.forEach(film => {
       const filmElement = createFilmElement(film);
       filmsContainer.appendChild(filmElement);

       fetchCharacters(film.characters)
         .then(characters => {
           const charactersContainer = filmElement.querySelector('.characters');
           charactersContainer.innerHTML = '';
           charactersContainer.appendChild(createCharactersList(characters));
         })
         .catch(error => {
           const charactersContainer = filmElement.querySelector('.characters');
           charactersContainer.innerHTML = `<p class="loading">Failed to load characters: ${error}</p>`;
         });
     });
   })
   .catch(error => {
     filmsContainer.innerHTML = `<p>Failed to load films: ${error}</p>`;
   });